package com.jpmc.codechalange.ui.schools

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.google.common.truth.Truth.assertThat
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import com.jpmc.codechalange.data.repository.SchoolsRepository
import com.jpmc.codechalange.model.School
import com.jpmc.codechalange.model.SchoolScore
import com.jpmc.codechalange.data.utils.APIResult
import com.jpmc.codechalange.utils.getOrAwaitValue
import com.jpmc.codechalange.viewmodel.SchoolViewModel
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking
import org.junit.Rule

import org.junit.Test

class SchoolViewModelTest {

    @get:Rule
    val instantExecutorRule = InstantTaskExecutorRule()

    private val repository: SchoolsRepository = mock()
    private val dispatcherIO: CoroutineDispatcher = Dispatchers.IO
    private val viewModel by lazy {
        SchoolViewModel(
            schoolsRepository = repository,
            dispatcherIo = dispatcherIO
        )
    }

    private val school = School("21K728",
        "LIBERATION DIPLOMA PLUS",
        "718-946-6812",
        "Brooklyn, NY",
        "scaraway@schools.nyc.gov")

    @Test
    fun refreshSchools() {
        runBlocking {

            val successResult = APIResult.Success(listOf(school))
            //Given
            whenever(repository.getSchools()).thenReturn(successResult)
            //When
            viewModel.getSchools()

            //Then
            val result = viewModel.schoolsList.getOrAwaitValue()
            assertThat(result).hasSize(1)
            val first = result.first()
            assertThat(first.dbn).isEqualTo("21K728")
            assertThat(first.name).isEqualTo("LIBERATION DIPLOMA PLUS")
            assertThat(first.phone).isEqualTo("718-946-6812")
            assertThat(first.location).isEqualTo("Brooklyn, NY")
            assertThat(first.email).isEqualTo("scaraway@schools.nyc.gov")
        }
    }

    @Test
    fun getSchoolDetails() {
        runBlocking {
            //Given
            val schoolScore = SchoolScore("21K728",
                "LIBERATION DIPLOMA PLUS",
                10,
                369.50,
                411.00,
                373.50)
            val listSuccess = APIResult.Success(listOf(schoolScore))
            whenever(repository.getSchoolScores(school.dbn)).thenReturn(listSuccess)

            //When
            viewModel.getSchoolDetails(school)

            //Then
            val result = viewModel.detailsLiveData.getOrAwaitValue()

            assertThat(result.dbn).isEqualTo(schoolScore.dbn)
            assertThat(result.name).isEqualTo(schoolScore.name)
            assertThat(result.students).isEqualTo(schoolScore.students)
            assertThat(result.avgMath).isEqualTo(schoolScore.avgMath)
            assertThat(result.avgReading).isEqualTo(schoolScore.avgReading)
            assertThat(result.avgWriting).isEqualTo(schoolScore.avgWriting)
        }
    }
}