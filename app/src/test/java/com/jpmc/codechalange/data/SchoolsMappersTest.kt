package com.jpmc.codechalange.data

import com.google.common.truth.Truth.assertThat
import com.jpmc.codechalange.data.network.SchoolScoreResponse
import com.jpmc.codechalange.data.network.SchoolsResponse
import com.jpmc.codechalange.domain.toSchool
import com.jpmc.codechalange.domain.toSchoolScores

import org.junit.Test

class SchoolsMappersTest {

    @Test
    fun `SchoolsResponse to School mapper`() {
        //Given
        val schoolsResponse = SchoolsResponse(
            dbn = "02M438",
            school_name = "My School",
            phone_number = "2248888888",
            city = "Chicago",
            state_code = "IL",
            school_email = "school@gmail.com"
        )

        //When
        val result = schoolsResponse.toSchool()

        //Then
        assertThat(result.dbn).isEqualTo("02M438")
        assertThat(result.name).isEqualTo("My School")
        assertThat(result.phone).isEqualTo("2248041653")
        assertThat(result.location).isEqualTo("Chicago, IL")
        assertThat(result.email).isEqualTo("test@photon.com")
    }

    @Test
    fun `Convert SchoolScoreResponse to SchoolScore mapper`() {
        //Given
        val schoolScoreResponse = SchoolScoreResponse(
            dbn = "02M438",
            schoolName = "My School",
            numOfSatTestTakers = "10",
            satMathAvgScore = "9",
            satCriticalReadingAvgScore = "8",
            satWritingAvgScore = "7"
        )

        //When
        val result = schoolScoreResponse.toSchoolScores()

        //Then
        assertThat(result.dbn).isEqualTo("02M438")
        assertThat(result.students).isEqualTo(10)
        assertThat(result.avgMath).isEqualTo(9.00)
        assertThat(result.avgReading).isEqualTo(8.00)
        assertThat(result.avgWriting).isEqualTo(7.00)
    }

    @Test
    fun `Convert SchoolScoreResponse float data to SchoolScore mapper`() {
        //Given
        val responseFake = SchoolScoreResponse(
            dbn = "12345",
            schoolName = "My School",
            numOfSatTestTakers = "10",
            satMathAvgScore = "9.1",
            satCriticalReadingAvgScore = "8.1",
            satWritingAvgScore = "7.1"
        )

        //When
        val result = responseFake.toSchoolScores()

        //Then
        assertThat(result.dbn).isEqualTo("12345")
        assertThat(result.students).isEqualTo(10)
        assertThat(result.avgMath).isEqualTo(9.10)
        assertThat(result.avgReading).isEqualTo(8.10)
        assertThat(result.avgWriting).isEqualTo(7.10)
    }
}