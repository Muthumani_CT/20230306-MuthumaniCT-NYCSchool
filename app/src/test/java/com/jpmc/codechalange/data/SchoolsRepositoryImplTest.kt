package com.jpmc.codechalange.data

import com.google.common.truth.Truth.assertThat
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import com.jpmc.codechalange.data.network.SchoolScoreResponse
import com.jpmc.codechalange.data.network.SchoolsResponse
import com.jpmc.codechalange.data.utils.APIResult
import kotlinx.coroutines.runBlocking
import okhttp3.ResponseBody.Companion.toResponseBody

import org.junit.Test
import retrofit2.Response

class SchoolsRepositoryImplTest {

    private val networkSchoolSource: NetworkSchoolSource = mock()
    private val repository by lazy { SchoolsRepositoryImpl(networkSchoolSource) }

    @Test
    fun `getSchools - Given network failure then repository return Resource Success`() {
        runBlocking {
            //Given
            val schoolsSat = listOf<SchoolsResponse>(mock(), mock())
            val response = Response.success(schoolsSat)
            whenever(networkSchoolSource.getSchools()).thenReturn(response)

            //When
            val resource = repository.getSchools()

            //Then
            assertThat(resource).isInstanceOf(APIResult.Success::class.java)
            resource as APIResult.Success
            assertThat(resource.data).isNotEmpty()
        }
    }

    @Test
    fun `getSchools - Given network failure then repository return Resource Failure`() {
        runBlocking {
            //Given
            val response = Response.error<List<SchoolsResponse>>(500, "".toResponseBody())
            whenever(networkSchoolSource.getSchools()).thenReturn(response)

            //When
            val resource = repository.getSchools()

            //Then
            assertThat(resource).isInstanceOf(APIResult.Failure::class.java)
            resource as APIResult.Failure
            assertThat(resource.data).isNull()
        }
    }

    @Test
    fun `getSchoolScore - Given network failure then repository return Resource Success`() {
        runBlocking {
            //Given
            val schoolsSat = listOf<SchoolScoreResponse>(mock(), mock())
            val response = Response.success(schoolsSat)
            whenever(networkSchoolSource.getSchoolScore("02M438")).thenReturn(response)

            //When
            val resource = repository.getSchoolScores("02M438")

            //Then
            assertThat(resource).isInstanceOf(APIResult.Success::class.java)
            resource as APIResult.Success
            assertThat(resource.data).isNotEmpty()
        }
    }

    @Test
    fun `getSchoolScore - Given network failure then repository return Resource Failure`() {
        runBlocking {
            //Given
            val response = Response.error<List<SchoolScoreResponse>>(403, "".toResponseBody())
            whenever(networkSchoolSource.getSchoolScore("02M438")).thenReturn(response)

            //When
            val resource = repository.getSchoolScores("02M438")

            //Then
            assertThat(resource).isInstanceOf(APIResult.Failure::class.java)
            resource as APIResult.Failure
            assertThat(resource.data).isNull()
        }
    }
}