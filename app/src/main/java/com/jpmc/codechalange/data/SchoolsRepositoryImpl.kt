package com.jpmc.codechalange.data

import com.jpmc.codechalange.data.utils.APIResult
import com.jpmc.codechalange.data.repository.SchoolsRepository
import com.jpmc.codechalange.domain.toSchool
import com.jpmc.codechalange.domain.toSchoolScores
import com.jpmc.codechalange.model.School
import com.jpmc.codechalange.model.SchoolScore

class SchoolsRepositoryImpl(
    private val remoteSchoolsDataSource: NetworkSchoolSource
) : SchoolsRepository {

    override suspend fun getSchools(): APIResult<List<School>> {
        val result = remoteSchoolsDataSource.getSchools()
        return when(result.isSuccessful){
            true -> APIResult.Success(result.body()?.map { it.toSchool() } ?: emptyList())
            false -> APIResult.Failure(null, Exception(result.message()))
        }
    }

    override suspend fun getSchoolScores(dbn : String): APIResult<List<SchoolScore>> {
        val result = remoteSchoolsDataSource.getSchoolScore(dbn)
        return when(result.isSuccessful){
            true -> APIResult.Success(result.body()?.map { response -> response.toSchoolScores() } ?: emptyList())
            false -> APIResult.Failure(null, Exception(result.message()))
        }
    }
}