package com.jpmc.codechalange.domain

import com.jpmc.codechalange.model.School
import com.jpmc.codechalange.model.SchoolScore
import com.jpmc.codechalange.data.network.SchoolScoreResponse
import com.jpmc.codechalange.data.network.SchoolsResponse

/**
 * Converts SchoolsResponse to School
 */
fun SchoolsResponse.toSchool() = School(
    dbn = dbn ?: "",
    name = school_name ?: "",
    phone = phone_number ?: "",
    location = "$city, $state_code",
    email = school_email ?: ""
)

/**
 * Converts SchoolScoreResponse to SchoolScores
 */
fun SchoolScoreResponse.toSchoolScores() = SchoolScore(
    dbn = dbn ?: "",
    name = schoolName ?: "",
    avgMath = satMathAvgScore?.toDoubleOrNull() ?: 0.0,
    avgReading = satCriticalReadingAvgScore?.toDoubleOrNull() ?: 0.0,
    avgWriting = satWritingAvgScore?.toDoubleOrNull() ?: 0.0,
    students = numOfSatTestTakers?.toIntOrNull() ?: 0
)