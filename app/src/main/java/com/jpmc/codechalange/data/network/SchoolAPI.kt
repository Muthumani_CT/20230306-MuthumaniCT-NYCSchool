package com.jpmc.codechalange.data.network

import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface SchoolAPI {
    @GET("s3k6-pzi2.json")
    suspend fun getSchoolsList(): Response<List<SchoolsResponse>>

    @GET("f9bf-2cp4.json")
    suspend fun getSchoolScores(@Query("dbn") dbn: String): Response<List<SchoolScoreResponse>>
}