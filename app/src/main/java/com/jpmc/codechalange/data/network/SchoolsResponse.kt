package com.jpmc.codechalange.data.network

import com.google.gson.annotations.SerializedName

data class SchoolsResponse(
    @SerializedName("dbn") val dbn: String?,
    @SerializedName("school_name") val school_name: String?,
    @SerializedName("phone_number") val phone_number: String?,
    @SerializedName("city") val city: String?,
    @SerializedName("state_code") val state_code: String?,
    @SerializedName("school_email") val school_email: String?
)
