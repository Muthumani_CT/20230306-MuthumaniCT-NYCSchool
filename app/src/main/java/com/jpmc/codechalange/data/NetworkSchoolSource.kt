package com.jpmc.codechalange.data

import com.jpmc.codechalange.data.network.SchoolScoreResponse
import com.jpmc.codechalange.data.network.SchoolsResponse
import com.jpmc.codechalange.data.network.SchoolAPI
import retrofit2.Response
import javax.inject.Inject

class NetworkSchoolSource @Inject constructor(
    private val schoolAPI: SchoolAPI
) {
    suspend fun getSchools(): Response<List<SchoolsResponse>> {
        return schoolAPI.getSchoolsList()
    }

    suspend fun getSchoolScore(dbn : String): Response<List<SchoolScoreResponse>> {
        return schoolAPI.getSchoolScores(dbn)
    }

    companion object {
        const val BASE_URL = "https://data.cityofnewyork.us/resource/"
    }
}