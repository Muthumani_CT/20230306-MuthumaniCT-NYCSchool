package com.jpmc.codechalange.data.repository

import com.jpmc.codechalange.model.School
import com.jpmc.codechalange.model.SchoolScore
import com.jpmc.codechalange.data.utils.APIResult

interface SchoolsRepository {
    suspend fun getSchools(): APIResult<List<School>>
    suspend fun getSchoolScores(dbn: String): APIResult<List<SchoolScore>>
}