package com.jpmc.codechalange.data.utils

sealed class APIResult<T>{
    data class Success<T>(val data: T): APIResult<T>()
    data class Failure<T>(val data: T?, val error: Throwable): APIResult<T>()
}