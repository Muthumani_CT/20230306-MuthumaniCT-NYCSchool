package com.jpmc.codechalange

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class SchoolApp: Application() {
}