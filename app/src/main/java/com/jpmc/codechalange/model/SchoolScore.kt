package com.jpmc.codechalange.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class SchoolScore(
    val dbn: String = "",
    val name: String = "",
    val students: Int = -1,
    val avgMath: Double = 0.0,
    val avgReading: Double = 0.0,
    val avgWriting: Double = 0.0
): Parcelable
