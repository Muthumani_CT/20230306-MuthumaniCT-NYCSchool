package com.jpmc.codechalange.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.jpmc.codechalange.databinding.SchoolViewBinding
import com.jpmc.codechalange.model.School

/**
 * Adapter to hold List of Schools
 */
class SchoolAdapter(
    private val onItemClick: (School) -> Unit
) : ListAdapter<School, SchoolAdapter.SchoolViewHolder>(SchoolDiff()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SchoolViewHolder {
        return SchoolViewHolder(
            SchoolViewBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: SchoolViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    class SchoolDiff : DiffUtil.ItemCallback<School>() {
        override fun areItemsTheSame(oldItem: School, newItem: School): Boolean {
            return oldItem.dbn == newItem.dbn
        }

        override fun areContentsTheSame(oldItem: School, newItem: School): Boolean {
            return oldItem == newItem
        }
    }

    inner class SchoolViewHolder(private val binding: SchoolViewBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(school: School) {
            with(binding) {
                schoolDetails.setOnClickListener { onItemClick(school) }
                schoolDbn.text = school.dbn
                schoolName.text = school.name
                schoolLocation.text = school.location
                schoolEmail.text = school.email
                schoolPhone.text = school.phone
            }
        }

    }
}