package com.jpmc.codechalange.ui

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.jpmc.codechalange.R
import com.jpmc.codechalange.databinding.ActivityDetailsBinding
import com.jpmc.codechalange.model.SchoolScore

class DetailActivity : AppCompatActivity() {

    private lateinit var binding: ActivityDetailsBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityDetailsBinding.inflate(layoutInflater)
        setContentView(binding.root)

        intent.extras?.getParcelable<SchoolScore>(SCHOOL_SCORES)?.let {
            updateUI(it)
        }
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    override fun onSupportNavigateUp(): Boolean {
        finish()
        return true
    }

    private fun updateUI(details: SchoolScore) {
        with(binding) {
            schoolName.text = details.name
            totalStudents.text = getString(R.string.total_students, details.students)
            readingScore.text = getString(R.string.reading_score, details.avgReading)
            writingScore.text = getString(R.string.writing_score, details.avgWriting)
            mathScore.text = getString(R.string.math_score, details.avgMath)
        }
    }


    companion object {
        private const val SCHOOL_SCORES = "SCHOOL_SCORES"

        /**
         * Create the intent to show the school score details
         */
        fun intent(context: Context, schoolDetails: SchoolScore) =
            Intent(context, DetailActivity::class.java).apply {
                putExtra(SCHOOL_SCORES, schoolDetails)
            }
    }
}