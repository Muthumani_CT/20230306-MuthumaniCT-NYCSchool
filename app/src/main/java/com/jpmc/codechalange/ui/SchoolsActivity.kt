package com.jpmc.codechalange.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.activity.viewModels
import com.jpmc.codechalange.viewmodel.SchoolViewModel
import com.jpmc.codechalange.ui.adapter.SchoolAdapter
import com.jpmc.codechalange.databinding.ActivitySchoolBinding
import com.jpmc.codechalange.model.School
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class SchoolsActivity : AppCompatActivity() {

    private lateinit var schoolBinding: ActivitySchoolBinding
    private val adapter by lazy { SchoolAdapter(::onItemClicked) }
    private val viewModel by viewModels<SchoolViewModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        schoolBinding = ActivitySchoolBinding.inflate(layoutInflater)
        setContentView(schoolBinding.root)

        schoolBinding.schoolRecyclerView.adapter = adapter

        setupObservers()
        viewModel.getSchools()
    }

    private fun setupObservers() {
        viewModel.schoolsList.observe(this){
            adapter.submitList(it)
            schoolBinding.progressBar.visibility = View.GONE
        }
        viewModel.detailsLiveData.observe(this){ details ->
            startActivity(DetailActivity.intent(this, details))
        }
        viewModel.apiErrorMessage.observe(this){ error ->
            schoolBinding.progressBar.visibility = View.GONE
            Toast.makeText(this, error, Toast.LENGTH_SHORT).show()
        }
    }

    /**
     * Called when the item on the list gets clicked
     */
    private fun onItemClicked(school: School){
        viewModel.getSchoolDetails(school)
    }
}