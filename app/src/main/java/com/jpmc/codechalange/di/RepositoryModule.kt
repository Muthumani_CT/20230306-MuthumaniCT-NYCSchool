package com.jpmc.codechalange.di

import com.jpmc.codechalange.data.NetworkSchoolSource
import com.jpmc.codechalange.data.SchoolsRepositoryImpl
import com.jpmc.codechalange.data.repository.SchoolsRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object RepositoryModule {
    @Singleton
    @Provides
    fun SchoolsRepository(dataSource: NetworkSchoolSource): SchoolsRepository = SchoolsRepositoryImpl(dataSource)
}