package com.jpmc.codechalange.di

import com.jpmc.codechalange.data.NetworkSchoolSource
import com.jpmc.codechalange.data.network.SchoolAPI
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object NetworkModule {
    @Singleton
    @Provides
    fun providesOkHttpClient(): OkHttpClient = OkHttpClient.Builder()
        .apply { 
            connectTimeout(30,TimeUnit.SECONDS)
            readTimeout(30, TimeUnit.SECONDS)
            addInterceptor(
                HttpLoggingInterceptor().apply {
                    level = HttpLoggingInterceptor.Level.BODY
                })
        }
    .build()

    @Singleton
    @Provides
    fun providesRetrofit(okHttpClient: OkHttpClient): Retrofit = Retrofit.Builder().client(okHttpClient).baseUrl(
        NetworkSchoolSource.BASE_URL
    ).addConverterFactory(GsonConverterFactory.create()).build()

    @Singleton
    @Provides
    fun providesSchoolAPI(retrofit: Retrofit): SchoolAPI = retrofit.create(SchoolAPI::class.java)

    @Singleton
    @Provides
    fun providesNetworkDatasource(schoolAPI: SchoolAPI): NetworkSchoolSource = NetworkSchoolSource(schoolAPI)
}


